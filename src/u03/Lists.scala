package u03

import u02.Optionals.Option
import u02.Optionals.Option.{None, Some}
import u02.Persons.Person
import u02.Persons.Person.Teacher


object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {

    case class Cons[E](head: E, tail: List[E]) extends List[E]

    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A, B](l: List[A])(mapper: A => B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match {
      case Cons(h, t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()
    }

    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(h, t) if n != 0 => drop(t, n - 1)
      case _ => l
    }

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case _ => Nil()
    }

    def map2[A, B](l: List[A])(mapper: A => B): List[B] = flatMap(l)(v => Cons(mapper(v), Nil()))

    //def filter2[A](l: List[A])(pred: A=>Boolean): List[A] = flatMap(l)(v => if (pred(v)) Cons(v, Nil()) else {Nil()} )

    def filter2[A](l: List[A])(pred: A => Boolean): List[A] = flatMap(l) {
      case v if pred(v) => Cons(v, Nil())
      case _ => Nil()
    }

    def max(l: List[Int]): Option[Int] = {

      def _max(l: List[Int], currentMax: Int): Option[Int] = l match {
        case Cons(h, t) if h > currentMax  => _max(t, h)
        case Cons(_, t) => _max(t, currentMax)
        case _ => Some(currentMax)

      }

      l match {
        case Nil() => None()
        case Cons(h, t) => _max(t, h)
      }

    }


    /*
    def getCourses(l: List[Person] ): List[String] = map(filter(l)({
      case Teacher(_, _) => true
      case _ => false
    }))(p => course(p))
     */
    def getCourses(l: List[Person]): List[String] = flatMap(l) {
      case Teacher(_, c) => Cons(c, Nil())
      case _ => Nil()

    }


    def foldLeft[A,B](l: List[A])(accumulator: B)(f: (B, A) => B): B = l match{
      case Nil() => accumulator
      case Cons(h, t) => foldLeft(t)(f(accumulator, h))(f)
    }

    /*
      Se sono nell'ultima posizione -> applico funzione tra elemento ed accumulatore
      Se sono in una lista ->
        applicco la fuzione alla testa e al risultato del folding a destra del resto della lista
       se sono in una lista vuota -> torno l'accumulatore

     */
    def foldRight[A,B](l: List[A])(accumulator: B)(f: (A, B) => B): B = l match{
      case Cons(h, t) if t == Nil() => f(h, accumulator)
      case Cons(h, t) => f(h, foldRight(t)(accumulator)(f))
      case _ => accumulator
    }


  }
}

object ListsMain extends App {

}