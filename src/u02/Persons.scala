package u02


object Persons {

  sealed trait Person
  object Person {

    case class Student(name: String, year: Int) extends Person

    case class Teacher(name: String, course: String) extends Person

    def name(p: Person): String = p match {
      case Student(n, _) => n
      case Teacher(n, _) => n
    }

    def course(p: Person): String = p match {
      case Teacher(_, c) => c
      case _ => ""
    }


  }

}
