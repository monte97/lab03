package u02

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

import u03.Lists.List._

import u03.Streams.Stream

class StreamTest {




  @Test def testDrop(): Unit = {
    var strResult = Stream.iterate(10)(_+1)   // {10,11,12,13,..}
    strResult = Stream.take(strResult)(10) /*10..20*/

    var str = Stream.iterate(0)(_+1)   // {0,1,2,3,..}
    str= Stream.drop(str)(10) //10..
    str = Stream.take(str)(10) /*10..20*/

    assertEquals(Stream.toList(strResult), Stream.toList(str))
  }

  @Test def testConstant(): Unit = {
    val result =
      Cons('x',
        Cons('x',
          Cons('x', Nil())))
    val emptyResult = Nil()

    assertEquals(emptyResult, Stream.toList(Stream.take(Stream.constant('x'))(0)))
    assertEquals(result, Stream.toList(Stream.take(Stream.constant('x'))(3)))

  }

  @Test def testFibo(): Unit = {
    val result = Cons(0 , Cons (1 , Cons (1 , Cons (2 , Cons (3 , Cons (5 , Cons (8 ,
      Cons (13 , Nil () ) ) ) ) ) ) ) )


  }
}
