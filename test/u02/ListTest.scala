package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Optionals.Option.{None, getOrElse}
import u03.Lists.List
import u03.Lists.List.Nil
import u03.Lists.List._
import u02.Persons.Person
import u02.Persons.Person.Teacher
import u02.Persons.Person.Student



class ListTest {
  val list = Cons(10, Cons(20, Cons(30, Nil())))
  val list2 =
    Cons(3,
      Cons(7,
        Cons(1,
          Cons(5, Nil()))))
  val listNeg = Cons(-10, Cons(-20, Cons(-30, Nil())))
  val emptyList = Nil()

  val listPerson: List[Person] =
    Cons(Teacher("a", "course1"),
      Cons(Teacher("b", "course2"),
      Cons(Student("c", 2020),
        Cons(Teacher("d", "course3"), Nil()))))

  val sum:(Int, Int) => Int = (x, y) => x+y

  @Test def testDrop(): Unit = {
    assertEquals(Cons(20, Cons(30, Nil())), drop(list, 1))
    assertEquals(Cons(30, Nil()), drop(list, 2))
    assertEquals(Nil(), drop(list, 3))
  }

  @Test def testFlatMap() : Unit = {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))),
      flatMap(list)(v => Cons(v+1, Nil())))

    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))),

      flatMap(list)(v => Cons(v+1, Cons(v+2, Nil()))))
  }

  /**
   * Map2 sould have the same bheaviour as the first version
   */
  @Test def testMap2(): Unit = {
    assertEquals(map(list)(v => v+1), map2(list)(v => v+1))
  }

  @Test def testFilter2(): Unit = {
    assertEquals(filter(list)(v => v > 10), filter2(list)(v => v > 10))
  }

  @Test def testMax(): Unit = {
    assertEquals(None(), max(Nil()))
    assertEquals(30, getOrElse(max(list), 0))
    assertEquals(-10, getOrElse(max(listNeg), 0))
  }

  @Test def testGetCourses(): Unit = {
    val listResult = Cons("course1", Cons("course2", Cons("course3", Nil())))

    assertEquals(listResult, getCourses(listPerson))

  }

  @Test def testFoldLeft(): Unit = {
    assertEquals(60, foldLeft(list)(0)(sum))
    assertEquals(6000, foldLeft(list)(1)(_ * _))
    assertEquals(-16, foldLeft(list2)(0)(_ - _))
    assertEquals(0,
      foldLeft(emptyList)(0)(sum))

  }
  @Test def testFoldRight(): Unit = {
    assertEquals(60, foldRight(list)(0)(_ + _))
    assertEquals(6000, foldRight(list)(1)(_ * _))
    assertEquals(-8, foldRight(list2)(0)(_ - _))
    assertEquals(0,
      foldRight(emptyList)(0)(sum))
  }

}
